package com.kof.doc.controller.vo;


import com.kof.doc.framework.db.dto.TableColumnDescDto;

import java.util.List;
import java.util.Map;

public class DatabaseExportVo {

	private Map<String, List<TableColumnDescDto>> columnList;

	private List<TableColumnVo.TableInfoVo> tableList;

	public Map<String, List<TableColumnDescDto>> getColumnList() {
		return columnList;
	}

	public void setColumnList(Map<String, List<TableColumnDescDto>> columnList) {
		this.columnList = columnList;
	}

	public List<TableColumnVo.TableInfoVo> getTableList() {
		return tableList;
	}

	public void setTableList(List<TableColumnVo.TableInfoVo> tableList) {
		this.tableList = tableList;
	}

}
